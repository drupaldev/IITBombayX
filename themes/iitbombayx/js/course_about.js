/**
* This file is part of IITBombayX-Drupal.
*
* IITBombayX-Drupal is free software: you can redistribute it and/or modify it 
* under the terms of the GNU General Public License as published by the Free 
* Software Foundation, either version 3 of the License, or (at your option) any
* later version.
*
* IITBombayX-Drupal is distributed in the hope that it will be useful,but 
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
* FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
* more details.
*
* You should have received a copy of the GNU General Public License along with
* IITBombayX-Drupal.  If not, see <http://www.gnu.org/licenses/>.

*******************************************************************************
*                                                                             *
* Purpose: This JS file provides the function for display course about page,  *
*          with different functionality for display buttons for Enrolled.     * 
*                                                                             *
* Created by: Mangesh Gharate                                                 *
*                                                                             *
*******************************************************************************
*/

(function ($, Drupal) {
    Drupal.behaviors.myfunction = {
        attach: function(context, settings) {
                var cid = $("#course_id").val();
                var cname = $("#course_name").val();
                var edxpath = $("#edx_site_path").val();
                var enrollment_start_date = $("#enrollment_start").val();
                var enrollment_end_date =$("#enrollment_end").val();

                var is_inviteonly = false;
                var is_loggedin = false;
                var already_enrolled = false;
                var is_enrollment_on = false;
                var future_enrollment = false;
                
                var about_string  ='';

                enrollstartstamp = new Date(enrollment_start_date).getTime();
                enrollendstamp = new Date(enrollment_end_date).getTime();
                current_date = new Date().getTime();

                if(enrollstartstamp <= current_date && enrollendstamp >= current_date)  is_enrollment_on = true; 
                if(enrollstartstamp > current_date) future_enrollment = true;


		var months = new Array("Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec");
		var ddSplit = enrollment_start_date.split('-');
		var enrollmentstartstring = months[ddSplit[1]-1] +' '+ ddSplit[2] + ', ' + ddSplit[0];

	        var decodedCookie = decodeURIComponent(document.cookie);
        	var ca = decodedCookie.split(';');
	        var edxinfo= {};

        	for(var i = 0; i <ca.length; i++)
         	{
                	var c = ca[i];
                        
	                while (c.charAt(0) == ' ') {
        	         c = c.substring(1);
        	        }
	
                	if(c.indexOf(edx_cookie_name) == 0) {
	                   if(c.substring(edx_cookie_name.length+1, c.length)=='true'){
                            is_loggedin = true;    
                            
                           }
        	        }
                }
  

           $.ajax({
           type: "GET",
	   url: edxpath+"/api/courses/v1/courses/"+cid,
	   cache: false,           
           success: function(data){
             var image_link = data.media.course_image.uri;
             var astrik_index = image_link.lastIndexOf("@"); 

             if(astrik_index == -1)
             image_link = image_link.substr(0, image_link.lastIndexOf("asset")+6);
             else
             image_link = image_link.substr(0, astrik_index+1);

             var regex = new RegExp('/static/', "igm");
             $("#about_data").html(data.overview.replace(regex, edxpath+""+image_link));
            }
           });

           $.ajax({
           type: "GET",
           url: edxpath+"/api/enrollment/v1/course/"+cid,
           cache: false,
           async:false,
           success: function(data){
            if(data.invite_only){
                is_inviteonly = true;
             }
            }
           });

          $.ajax({
           type: "GET",
           url: edxpath+"/api/enrollment/v1/enrollment/"+cid,
           cache: false,
           async:false,
           xhrFields: { withCredentials: true },  
           success: function(data){
             if(data.is_active) already_enrolled =true;
          }
         }); 

/*
                var is_inviteonly = false;
                var is_loggedin = false;
                var already_enrolled = false;
                var is_enrollment_on = false;
                var future_enrollment = true;
*/


         if(is_loggedin && already_enrolled){
              $('.course_enroll_btn').html('<a href="'+edxpath+'/courses/'+cid+'/info"><span class="btn disabled">You are enrolled in this course</span>&nbsp;<span class="btn btn-primary">View Course</strong></span>');
          }
         else if(is_inviteonly){ 
              $('.course_enroll_btn').html('<span class="btn disabled">Enrollment in this course is by invitation only</span>');
          } 
         else if(is_enrollment_on){
             $('.course_enroll_btn').html('<a href="#" class="btn btn-primary ajaxCall">Enroll in '+cname+' </a><div id="register_error"/>');
          }
         else if(future_enrollment){
           $('.course_enroll_btn').html('<span class="btn disabled">Enrollment will start from '+enrollmentstartstring+' </span>');
          }
         else{
           $('.course_enroll_btn').html('<span class="btn disabled">Enrollment date is over.</span>');
         }

           
    $(".ajaxCall").click(function(event) {
      $.ajax({
        type:'POST',
        url: edxpath+'/change_enrollment',
        data:$('#class_enroll_form').serialize(),
        success: function(responseData, textStatus, xhr) {
           if (xhr.responseText == "") {
	     // location.href = edxpath+"/dashboard";
           }
           else {
            //  location.href = xhr.responseText;
           }
        },
        error: function(xhr, textStatus, errorThrown) {
           if (xhr.status == 403) {
             location.href = edxpath+"/register?course_id="+cid+"&enrollment_action=enroll";
           } else {
             $('#register_error').html((xhr.responseText ? xhr.responseText : "An error occurred. Please try again later.")).css("display", "block");
           }
        }
      });
    });
   }
  }
})(jQuery, Drupal);

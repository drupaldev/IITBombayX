<?php
/*
# This file is part of IIMBX-Drupal.
#
# IIMBX-Drupal is free software: you can redistribute it and/or modify it 
# under the terms of the GNU General Public License as published by the Free 
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# IIMBX-Drupal is distributed in the hope that it will be useful,but 
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
# more details.
#
# You should have received a copy of the GNU General Public License along with
# IIMBX-Drupal.  If not, see <http://www.gnu.org/licenses/>.

###############################################################################
#                                                                             #
# Purpose: It contains logic and main program of module.                      #
#                                                                             #
# Created by: Mangesh Gharate                                                 #
#                                                                             #
# Date: 19-jul-2017                                                           #
#                                                                             #
#                                                                             #
# Change Log:                                                                 #
# Version Date     By             Description                                 #
# --------------------------------------------------------------------------- #
# 1.0     19-07-17  Mangesh G      Initial Version                            #
###############################################################################

*/
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\node\Entity\Node;
use Drupal\node\NodeInterface;
use \Drupal\file\Entity\File;
use Drupal\Component\Serialization\Yaml;

/* function to get the YML file */
   function get_config_filepath()
   {
       return getcwd()."/".drupal_get_path('module','import_courses')."/config.yml";
   }

/* Function to Fetch data through API and decode it in json format */
   function get_json_dump($url)
   {
         $response = \Drupal::httpClient()->get($url, array('headers' => array('Accept' => 'application/json')));
         $res_status = $response->getStatusCode();
         $response = $response->getBody()->getContents();
         $result = json_decode($response,true);
         $result_array[0] = $res_status;
         $result_array[1] = $result;
 
         return $result_array;
   }

/*This function is used to fetch data from nested array e.g Image_url data but here we are using it to fetch all values */
   function deriveValue($CourseInfo,$TreeList)
   {
          $tmp=$CourseInfo;
          $TreePath=explode(',',$TreeList);
          //var_dump($TreePath);
           foreach($TreePath as $indx)
           {
              $tmp=$tmp[$indx];
           }
          return $tmp;
   }

/** Implements hook_cron() **/
   function import_courses_cron()
   {
      global $node_log_str;
      global $node_report_str;
      global $node_url_array;
 
      $api_endpath = \Drupal::config('node.settings')->get('edx_site_path');

      // get the contents of config file and dump the yml contents in map_array
      $config_file_content = file_get_contents(get_config_filepath());
      $map_array = Yaml::decode($config_file_content);
      $next_page_number = 1;
      $last_page_number = 1;
      $useridi = \Drupal::entityQuery('user')->condition('name', 'ContentManager')->execute(); 
      $author_id = reset($useridi);     
      while($next_page_number <= $last_page_number)
      {
         
        $result_arr = get_json_dump($api_endpath."/api/courses/v1/courses/?page_size=100&page=".$next_page_number);

        if($result_arr[0] == 200){
           $result = $result_arr[1];
           $next_page_number++;
           $pages = $result[$map_array['pagination']];
           $last_page_number = $pages['num_pages'];

           foreach ($result[$map_array['course_list']] as $course)
            {
              $course_code = deriveValue($course,$map_array['course_code']);
              $course_url = deriveValue($course,$map_array['link_url']);
              $nids = \Drupal::entityQuery('node')
              ->condition('field_course_link', $course_url)
              ->condition('type', 'course')
              ->range(0, 1)
              ->execute();
              $nodeCount = count($nids);
              $node_url_array[] = $course_url;

              $current_date_stamp = strtotime(date("Y-m-d"));
              $start_date_api = processRawdate(deriveValue($course,$map_array['start_date']));
              $start_date_stamp = strtotime(date("Y-m-d",$start_date_api));             

              if(deriveValue($course,$map_array['end_date']))
                $end_date_stamp = processRawdate(deriveValue($course,$map_array['end_date']));
              else 
                $end_date_stamp = 4102441200;

 
              if(deriveValue($course,$map_array['enrollment_start'])) $enrollment_start_stamp = processRawdate(deriveValue($course,$map_array['enrollment_start']));
              else $enrollment_start_stamp = $start_date_stamp;

              if(deriveValue($course,$map_array['enrollment_end'])) $enrollment_end_stamp = processRawdate(deriveValue($course,$map_array['enrollment_end']));
              else $enrollment_end_stamp = $end_date_stamp;

              if(deriveValue($course, $map_array['announcement']) && trim(deriveValue($course, $map_array['announcement']))!='')
                $display_string = deriveValue($course, $map_array['announcement']);
              else $display_string   =  date("d M Y",$start_date_stamp);

              if(deriveValue($course,$map_array['start_date']))
                $start_date = date("Y-m-d",$start_date_stamp);
              else $start_date = '';

              if(deriveValue($course,$map_array['end_date']))
                $end_date = date("Y-m-d", $end_date_stamp);
              else $end_date ='';


              if($start_date_stamp <= $current_date_stamp && ($current_date_stamp <= $end_date_stamp))
                $time_line = "Ongoing";
              elseif($current_date_stamp > $end_date_stamp)
                $time_line = "Archived";
              else
                $time_line = "Launching Soon";

                       
             $enrollment_start = date("Y-m-d", $enrollment_start_stamp);
             $enrollment_end   = date("Y-m-d", $enrollment_end_stamp);
 
             $course_code_var  = deriveValue($course,$map_array['course_code']);
             $link_url_var     = deriveValue($course,$map_array['link_url']);
             $image_url_var    = deriveValue($course,$map_array['image_url']);
             $course_name_var  = deriveValue($course, $map_array['course_name']);
             $pacing  = deriveValue($course, $map_array['pacing']);
             $effort  = deriveValue($course, $map_array['effort']);
             $video  = deriveValue($course, $map_array['video']);


              try
               {
                 if($nodeCount==0)
                   {
                      // If course NOT exist create new course (content)

                      $node = Node::create
                           ([
                                 'type' => 'course',
                                 'title' => $link_url_var,
                                 'status' => 1,
                                 'promote' => 0,
                                 'comment' => 1,
                                 'uid' => $author_id,
                                 'field_course_code' => $course_code_var,
                                 'field_course_link' => $link_url_var,
                                 'field_course_name' => $course_name_var,
                                 'field_course_image' => $image_url_var,
                                 'field_course_start' => $start_date,
                                 'field_course_end' => $end_date,
                                 'field_start_time_stamp' => $start_date_stamp,
                                 'field_display_string' => $display_string,
                                 'field_enrollment_start' => $enrollment_start,
                                 'field_enrollment_end' => $enrollment_end,
                                 'field_course_pacing' => $pacing,
                                 'field_course_effort' => $effort,
                                 'field_course_video' => $video,
                                 'field_time_line'=> $time_line
                               ]);

                       $node->save();

               }
              else
               {
                      $current_node_id = current($nids);

                     //If course already exist and updated any field then update course (content)

                      $node = Node::load($current_node_id);
                     //$node->status = $status;
                      $node->title = $link_url_var;
 
                      if($node->field_course_code->getValue()[0]['value']!=$course_code_var ||
                         $node->field_course_link->getValue()[0]['value']!=$link_url_var ||
                         $node->field_course_name->getValue()[0]['value']!=$course_name_var||
                         $node->field_course_image->getValue()[0]['value']!=$image_url_var||
                         $node->field_course_start->getValue()[0]['value']!=$start_date||
                         $node->field_course_end->getValue()[0]['value']!=$end_date||
                         $node->field_start_time_stamp->getValue()[0]['value']!=$start_date_stamp||
                         $node->field_display_string->getValue()[0]['value']!=$display_string||
                         $node->field_enrollment_start->getValue()[0]['value']!=$enrollment_start||
                         $node->field_enrollment_end->getValue()[0]['value']!=$enrollment_end||
                         $node->field_course_pacing->getValue()[0]['value']!=$pacing||
                         $node->field_course_effort->getValue()[0]['value']!=$effort||
                         $node->field_course_video->getValue()[0]['value']!=$video||
                         $node->field_time_line->getValue()[0]['value']!=$time_line)
        
                      {
                        $node->field_course_code= $course_code_var;
                        $node->field_course_link= $link_url_var;
                        $node->field_course_name= $course_name_var;
                        $node->field_course_image= $image_url_var;
                        $node->field_course_start= $start_date;
                        $node->field_course_end= $end_date;
                        $node->field_start_time_stamp= $start_date_stamp;
                        $node->field_display_string = $display_string;
                        $node->field_enrollment_start = $enrollment_start;
                        $node->field_enrollment_end = $enrollment_end;
                        $node->field_course_pacing = $pacing;
                        $node->field_course_effort = $effort;
                        $node->field_course_video = $video;
                        $node->field_time_line = $time_line;
                        $node->save();
                      }
               }
            }

            catch (exception $e)
            {
                 // update_log($e,'error');
                 $node_log_str .="  Error: ".$e."\n";
            }
          }


        }
       else{
        return false;
       }
     }
unpublish_courses();

  drupal_flush_all_caches();
} 
 //Courses removed from openEdx will unpublished from drupal by this function
 
 function unpublish_courses()
   {
        global $node_url_array;
        $course_unpublished = 0;

        $available_nids = \Drupal::entityQuery('node')->condition('type', 'course')->execute();

        foreach ($available_nids as $available_nid)
             {
                $nodex = Node::load($available_nid);
                $node_link_url = $nodex->field_course_link->value;
 
                if(!in_array($node_link_url, $node_url_array))
                {
                  $nodex->status = 0;
                  $nodex->save();
                }
            }
   }

function processRawdate($rawdate)
{

           $dar = explode('T',str_replace('Z','',$rawdate));
           $dar1 = explode('-',$dar[0]);
           $dar2 = explode(':',$dar[1]);
   
           return mktime($dar2[0],$dar2[1],$dar2[2],$dar1[1],$dar1[2] ,$dar1[0]);
}


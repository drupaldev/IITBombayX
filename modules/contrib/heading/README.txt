Heading
=======
The heading module adds a new field type containing a text field and a heading
size. The field will be formatted as a HTML heading (h1 - h6).

This field type is created in first place to be used within paragraphs.


Installation
-------------
1. Download and extract this module to the modules/contrib directory
or download using composer.
2. Enable the module.


Usage
-----
Create a content type or paragraph type and add the Text > Heading field to it.

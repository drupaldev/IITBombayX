<?php

namespace Drupal\heading\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\StringTranslation\TranslationInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * A widget bar.
 *
 * @FieldWidget(
 *   id = "heading",
 *   label = @Translation("Heading"),
 *   field_types = {
 *     "heading"
 *   }
 * )
 */
class HeadingWidget extends WidgetBase implements ContainerFactoryPluginInterface {

  /**
   * Constructs a EntranceFeeWidget object.
   *
   * @param string $plugin_id
   *   The plugin_id for the formatter.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the formatter is associated.
   * @param array $settings
   *   The widget settings.
   * @param array $third_party_settings
   *   Any third party settings.
   * @param \Drupal\Core\StringTranslation\TranslationInterface $translation
   *   The String translation.
   */
  public function __construct(
    $plugin_id,
    $plugin_definition,
    FieldDefinitionInterface $field_definition,
    array $settings,
    array $third_party_settings,
    TranslationInterface $translation
  ) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $third_party_settings);
    $this->setStringTranslation($translation);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $stringTranslation = $container->get('string_translation');
    /* @var $stringTranslation TranslationInterface */

    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['third_party_settings'],
      $stringTranslation
    );
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element['#attached']['library'][] = 'heading/widget';
    $element['container'] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => ['heading-widget--container'],
      ],
    ];

    $element['container']['text'] = [
      '#type' => 'textfield',
      '#title' => $this->getLabel(),
      '#default_value' => $items[$delta]->text,
    ];

    $size_options = $this->getTypes();
    $size_options_keys = array_keys($size_options);
    $size_default = isset($items[$delta]->size)
      ? $items[$delta]->size
      : reset($size_options_keys);

    $element['container']['size'] = [
      '#type' => 'select',
      '#title' => $this->t('Size'),
      '#default_value' => $size_default,
      '#options' => $size_options,
    ];

    $element['container']['underline'] = [
	'#type' => 'checkbox',
        '#title' => $this->t('Underline'),
	'#default_value' => $items[$delta]->underline,
	'#prefix' => "<div class=\"publish_checkbox\"",
	'#suffix' => "</div>",
    ];


    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function massageFormValues(array $values, array $form, FormStateInterface $form_state) {
    foreach ($values as $delta => $data) {
      $values[$delta]['text'] = $data['container']['text'];
      $values[$delta]['size'] = $data['container']['size'];
      $values[$delta]['underline'] = $data['container']['underline'];
      unset($values[$delta]['container']);
    }

    return $values;
  }

  /**
   * Get the label for the text field.
   *
   * @return string
   *   The label string.
   */
  protected function getLabel() {
    return $this->fieldDefinition->getSetting('label');
  }

  /**
   * Get the available heading types.
   *
   * @return array
   *   The heading size labels keyed by their size (h1-h6).
   */
  protected function getTypes() {
    $allowed_sizes = array_filter($this->fieldDefinition->getSetting('allowed_sizes'));
    $sizes = [
      'h1' => $this->t('Heading 1')->render(),
      'h2' => $this->t('Heading 2')->render(),
      'h3' => $this->t('Heading 3')->render(),
      'h4' => $this->t('Heading 4')->render(),
      'h5' => $this->t('Heading 5')->render(),
      'h6' => $this->t('Heading 6')->render(),
    ];

    return array_intersect_key($sizes, $allowed_sizes);
  }

}

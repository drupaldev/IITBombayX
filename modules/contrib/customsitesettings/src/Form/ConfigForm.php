<?php
	namespace Drupal\customsitesettings\Form;
	use Drupal\Core\Form\ConfigFormBase;
	use Drupal\Core\Form\FormStateInterface;
	use Drupal\Core\Url;
	use Drupal\Component\Utility\SafeMarkup;	  

	class ConfigForm extends ConfigFormBase {	 
	 // Method that renders form id.
	 public function getFormId() {
	   return 'configform_customsitesettings_form';
	 }

	 // Instead of hook_form.

	 public function buildForm(array $form, FormStateInterface $form_state) {

        $form = parent::buildForm($form, $form_state);     

        $form['edx_site_information'] = [
              '#type' => 'details',
              '#title' => t('Edx Site Settings'),
              '#open' => TRUE,
            ];

        $form['course_about_page_settings'] = [
              '#type' => 'details',
              '#title' => t('Course About Page Settings'),
              '#open' => TRUE,
            ];

        $form['edx_site_information']['edx_site_path'] = array(
                '#type' => 'textfield',
                '#title' => t('Edx Site Path'),
                '#description' => t('This path is used to display course images on search course page.'),
                '#default_value' => \Drupal::config('node.settings')->get('edx_site_path'),
                '#group' => t('EDX SITE CONFIGURATION')
        );

        $form['edx_site_information']['user_cookie_name'] = array(
                '#type' => 'textfield',
                '#title' => t('User Info Cookie Name'),
                '#description' => t('This Cookie name is used to access logged in user information.'),
                '#default_value' => \Drupal::config('node.settings')->get('user_cookie_name'),
                '#group' => t('EDX SITE CONFIGURATION')
        );

        $form['edx_site_information']['edx_cookie_name'] = array(
                '#type' => 'textfield',
                '#title' => t('Edx Logged Cookie Name'),
                '#description' => t('This Cookie name is used to access logged info.'),
                '#default_value' => \Drupal::config('node.settings')->get('edx_cookie_name'),
                '#group' => t('EDX SITE CONFIGURATION')
        ); 

        $form['course_about_page_settings']['label_optional'] = array(
	        '#markup' => '<b>Information will be shown on course page.</b>',               
        );   

        $form['course_about_page_settings']['show_course_number'] = array(
                '#type' => 'checkbox',
                '#title' => t('Course Number'),
                '#default_value' => \Drupal::config('node.settings')->get('show_course_number')
        );     
         

        $form['course_about_page_settings']['show_classes_start'] = array(
                '#type' => 'checkbox',
                '#title' => t('Classes Start'),
                '#default_value' => \Drupal::config('node.settings')->get('show_classes_start')
        );

        $form['course_about_page_settings']['show_classes_end'] = array(
                '#type' => 'checkbox',
                '#title' => t('Classes End'),
                '#default_value' => \Drupal::config('node.settings')->get('show_classes_end')
        );

        $form['course_about_page_settings']['show_course_efforts'] = array(
                '#type' => 'checkbox',
                '#title' => t('Efforts'),
                '#default_value' => \Drupal::config('node.settings')->get('show_course_efforts')
        );

        $form['course_about_page_settings']['show_course_domain'] = array(
                '#type' => 'checkbox',
                '#title' => t('Domain'),
                '#default_value' => \Drupal::config('node.settings')->get('show_course_domain')
        );

        $form['course_about_page_settings']['show_course_programme'] = array(
                '#type' => 'checkbox',
                '#title' => t('Programme'),
                '#default_value' => \Drupal::config('node.settings')->get('show_course_programme')
        );
        $form['course_about_page_settings']['show_course_pacing'] = array(
                '#type' => 'checkbox',
                '#title' => t('Pacing'),
                '#default_value' => \Drupal::config('node.settings')->get('show_course_pacing')
        );

        $form['course_about_page_settings']['show_course_timeline'] = array(
                '#type' => 'checkbox',
                '#title' => t('Timeline'),
                '#default_value' => \Drupal::config('node.settings')->get('show_course_timeline')
        );

        $form['course_about_page_settings']['show_course_moocs'] = array(
                  '#type' => 'checkbox',
                  '#title' => t('Types Of Mooc'),
                  '#default_value' => \Drupal::config('node.settings')->get('show_course_moocs')
        );

        $form['course_about_page_settings']['note_optional'] = array(
               '#markup' => '<b>Note :</b> Pleae clear cache after saving configuration. <a target="_blank" href="development/performance">Click here to clear cache.</a> ',
        );

     return $form;	   
  }   


	 // Instead of hook_form_submit.

	 public function submitForm(array &$form, FormStateInterface $form_state) {

          \Drupal::configFactory()->getEditable('node.settings')
            ->set('edx_site_path', $form_state->getValue('edx_site_path'))
            ->save();

          \Drupal::configFactory()->getEditable('node.settings')
            ->set('edx_cookie_name', $form_state->getValue('edx_cookie_name'))
            ->save();

          \Drupal::configFactory()->getEditable('node.settings')
            ->set('user_cookie_name', $form_state->getValue('user_cookie_name'))
            ->save();	   

          \Drupal::configFactory()->getEditable('node.settings')
          ->set('show_course_number', $form_state->getValue('show_course_number'))
          ->save();
          
          \Drupal::configFactory()->getEditable('node.settings')
          ->set('show_classes_start', $form_state->getValue('show_classes_start'))
          ->save();

          \Drupal::configFactory()->getEditable('node.settings')
          ->set('show_classes_end', $form_state->getValue('show_classes_end'))
          ->save();

          \Drupal::configFactory()->getEditable('node.settings')
          ->set('show_course_efforts', $form_state->getValue('show_course_efforts'))
          ->save();

           \Drupal::configFactory()->getEditable('node.settings')
          ->set('show_course_domain', $form_state->getValue('show_course_domain'))
          ->save();

          \Drupal::configFactory()->getEditable('node.settings')
          ->set('show_course_programme', $form_state->getValue('show_course_programme'))
          ->save();

          \Drupal::configFactory()->getEditable('node.settings')
          ->set('show_course_pacing', $form_state->getValue('show_course_pacing'))
          ->save();

          \Drupal::configFactory()->getEditable('node.settings')
          ->set('show_course_timeline', $form_state->getValue('show_course_timeline'))
          ->save();

          \Drupal::configFactory()->getEditable('node.settings')
          ->set('show_course_moocs', $form_state->getValue('show_course_moocs'))
          ->save();

          drupal_set_message(t('The configuration options have been saved. <a target="_blank" href="development/performance">Click here to clear cache.</a> '));
       }

       // An array of names of configuration objects that are available for editing.

      protected function getEditableConfigNames() {
        return ['node.settings'];
      }  
  
	}

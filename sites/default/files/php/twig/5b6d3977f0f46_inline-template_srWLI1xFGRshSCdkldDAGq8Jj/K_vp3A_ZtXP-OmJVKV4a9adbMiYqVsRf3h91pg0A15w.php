<?php

/* {# inline_template_start #}{% if field_link_ is empty  %}
 <img alt="img1" class="img-responsive hidden-md hidden-sm hidden-xs" data-entity-type="file" data-entity-uuid="1f2b7643-1cc3-4162-a5fa-c57fd89f694a" src="{{ field_slide_image_1179_x_359_ }}"><img alt="img2" class="img-responsive hidden-lg hidden-sm hidden-xs" data-entity-type="file" data-entity-uuid="04c5ee0f-6dc7-4143-a798-6d6ade284969" src="{{ field_slide_image_970_x_359_ }}"><img alt="img3" class="img-responsive hidden-md hidden-lg" data-entity-type="file" data-entity-uuid="24ff748d-9f21-42bf-9921-90cf0680de00" src="{{ field_slide_image_750_x_359_ }}">
{% else %}
   <a href="{{ field_link_ }}"><img alt="img1" class="img-responsive hidden-md hidden-sm hidden-xs" data-entity-type="file" data-entity-uuid="1f2b7643-1cc3-4162-a5fa-c57fd89f694a" src="{{ field_slide_image_1179_x_359_ }}"></a><a href="{{ field_link_ }}"><img alt="img2" class="img-responsive hidden-lg hidden-sm hidden-xs" data-entity-type="file" data-entity-uuid="04c5ee0f-6dc7-4143-a798-6d6ade284969" src="{{ field_slide_image_970_x_359_ }}"></a><a href="{{ field_link_ }}"><img alt="img3" class="img-responsive hidden-md hidden-lg" data-entity-type="file" data-entity-uuid="24ff748d-9f21-42bf-9921-90cf0680de00" src="{{ field_slide_image_750_x_359_ }}"></a>

{% endif %} */
class __TwigTemplate_e96fb3256db698305e30ac16e0f073a73e33cde3c271a8c040e4ae3e6e3c31e0 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $tags = array("if" => 1);
        $filters = array();
        $functions = array();

        try {
            $this->env->getExtension('Twig_Extension_Sandbox')->checkSecurity(
                array('if'),
                array(),
                array()
            );
        } catch (Twig_Sandbox_SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof Twig_Sandbox_SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

        // line 1
        if (twig_test_empty(($context["field_link_"] ?? null))) {
            // line 2
            echo " <img alt=\"img1\" class=\"img-responsive hidden-md hidden-sm hidden-xs\" data-entity-type=\"file\" data-entity-uuid=\"1f2b7643-1cc3-4162-a5fa-c57fd89f694a\" src=\"";
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["field_slide_image_1179_x_359_"] ?? null), "html", null, true));
            echo "\"><img alt=\"img2\" class=\"img-responsive hidden-lg hidden-sm hidden-xs\" data-entity-type=\"file\" data-entity-uuid=\"04c5ee0f-6dc7-4143-a798-6d6ade284969\" src=\"";
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["field_slide_image_970_x_359_"] ?? null), "html", null, true));
            echo "\"><img alt=\"img3\" class=\"img-responsive hidden-md hidden-lg\" data-entity-type=\"file\" data-entity-uuid=\"24ff748d-9f21-42bf-9921-90cf0680de00\" src=\"";
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["field_slide_image_750_x_359_"] ?? null), "html", null, true));
            echo "\">
";
        } else {
            // line 4
            echo "   <a href=\"";
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["field_link_"] ?? null), "html", null, true));
            echo "\"><img alt=\"img1\" class=\"img-responsive hidden-md hidden-sm hidden-xs\" data-entity-type=\"file\" data-entity-uuid=\"1f2b7643-1cc3-4162-a5fa-c57fd89f694a\" src=\"";
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["field_slide_image_1179_x_359_"] ?? null), "html", null, true));
            echo "\"></a><a href=\"";
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["field_link_"] ?? null), "html", null, true));
            echo "\"><img alt=\"img2\" class=\"img-responsive hidden-lg hidden-sm hidden-xs\" data-entity-type=\"file\" data-entity-uuid=\"04c5ee0f-6dc7-4143-a798-6d6ade284969\" src=\"";
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["field_slide_image_970_x_359_"] ?? null), "html", null, true));
            echo "\"></a><a href=\"";
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["field_link_"] ?? null), "html", null, true));
            echo "\"><img alt=\"img3\" class=\"img-responsive hidden-md hidden-lg\" data-entity-type=\"file\" data-entity-uuid=\"24ff748d-9f21-42bf-9921-90cf0680de00\" src=\"";
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["field_slide_image_750_x_359_"] ?? null), "html", null, true));
            echo "\"></a>

";
        }
    }

    public function getTemplateName()
    {
        return "{# inline_template_start #}{% if field_link_ is empty  %}
 <img alt=\"img1\" class=\"img-responsive hidden-md hidden-sm hidden-xs\" data-entity-type=\"file\" data-entity-uuid=\"1f2b7643-1cc3-4162-a5fa-c57fd89f694a\" src=\"{{ field_slide_image_1179_x_359_ }}\"><img alt=\"img2\" class=\"img-responsive hidden-lg hidden-sm hidden-xs\" data-entity-type=\"file\" data-entity-uuid=\"04c5ee0f-6dc7-4143-a798-6d6ade284969\" src=\"{{ field_slide_image_970_x_359_ }}\"><img alt=\"img3\" class=\"img-responsive hidden-md hidden-lg\" data-entity-type=\"file\" data-entity-uuid=\"24ff748d-9f21-42bf-9921-90cf0680de00\" src=\"{{ field_slide_image_750_x_359_ }}\">
{% else %}
   <a href=\"{{ field_link_ }}\"><img alt=\"img1\" class=\"img-responsive hidden-md hidden-sm hidden-xs\" data-entity-type=\"file\" data-entity-uuid=\"1f2b7643-1cc3-4162-a5fa-c57fd89f694a\" src=\"{{ field_slide_image_1179_x_359_ }}\"></a><a href=\"{{ field_link_ }}\"><img alt=\"img2\" class=\"img-responsive hidden-lg hidden-sm hidden-xs\" data-entity-type=\"file\" data-entity-uuid=\"04c5ee0f-6dc7-4143-a798-6d6ade284969\" src=\"{{ field_slide_image_970_x_359_ }}\"></a><a href=\"{{ field_link_ }}\"><img alt=\"img3\" class=\"img-responsive hidden-md hidden-lg\" data-entity-type=\"file\" data-entity-uuid=\"24ff748d-9f21-42bf-9921-90cf0680de00\" src=\"{{ field_slide_image_750_x_359_ }}\"></a>

{% endif %}";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  60 => 4,  50 => 2,  48 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{# inline_template_start #}{% if field_link_ is empty  %}
 <img alt=\"img1\" class=\"img-responsive hidden-md hidden-sm hidden-xs\" data-entity-type=\"file\" data-entity-uuid=\"1f2b7643-1cc3-4162-a5fa-c57fd89f694a\" src=\"{{ field_slide_image_1179_x_359_ }}\"><img alt=\"img2\" class=\"img-responsive hidden-lg hidden-sm hidden-xs\" data-entity-type=\"file\" data-entity-uuid=\"04c5ee0f-6dc7-4143-a798-6d6ade284969\" src=\"{{ field_slide_image_970_x_359_ }}\"><img alt=\"img3\" class=\"img-responsive hidden-md hidden-lg\" data-entity-type=\"file\" data-entity-uuid=\"24ff748d-9f21-42bf-9921-90cf0680de00\" src=\"{{ field_slide_image_750_x_359_ }}\">
{% else %}
   <a href=\"{{ field_link_ }}\"><img alt=\"img1\" class=\"img-responsive hidden-md hidden-sm hidden-xs\" data-entity-type=\"file\" data-entity-uuid=\"1f2b7643-1cc3-4162-a5fa-c57fd89f694a\" src=\"{{ field_slide_image_1179_x_359_ }}\"></a><a href=\"{{ field_link_ }}\"><img alt=\"img2\" class=\"img-responsive hidden-lg hidden-sm hidden-xs\" data-entity-type=\"file\" data-entity-uuid=\"04c5ee0f-6dc7-4143-a798-6d6ade284969\" src=\"{{ field_slide_image_970_x_359_ }}\"></a><a href=\"{{ field_link_ }}\"><img alt=\"img3\" class=\"img-responsive hidden-md hidden-lg\" data-entity-type=\"file\" data-entity-uuid=\"24ff748d-9f21-42bf-9921-90cf0680de00\" src=\"{{ field_slide_image_750_x_359_ }}\"></a>

{% endif %}", "{# inline_template_start #}{% if field_link_ is empty  %}
 <img alt=\"img1\" class=\"img-responsive hidden-md hidden-sm hidden-xs\" data-entity-type=\"file\" data-entity-uuid=\"1f2b7643-1cc3-4162-a5fa-c57fd89f694a\" src=\"{{ field_slide_image_1179_x_359_ }}\"><img alt=\"img2\" class=\"img-responsive hidden-lg hidden-sm hidden-xs\" data-entity-type=\"file\" data-entity-uuid=\"04c5ee0f-6dc7-4143-a798-6d6ade284969\" src=\"{{ field_slide_image_970_x_359_ }}\"><img alt=\"img3\" class=\"img-responsive hidden-md hidden-lg\" data-entity-type=\"file\" data-entity-uuid=\"24ff748d-9f21-42bf-9921-90cf0680de00\" src=\"{{ field_slide_image_750_x_359_ }}\">
{% else %}
   <a href=\"{{ field_link_ }}\"><img alt=\"img1\" class=\"img-responsive hidden-md hidden-sm hidden-xs\" data-entity-type=\"file\" data-entity-uuid=\"1f2b7643-1cc3-4162-a5fa-c57fd89f694a\" src=\"{{ field_slide_image_1179_x_359_ }}\"></a><a href=\"{{ field_link_ }}\"><img alt=\"img2\" class=\"img-responsive hidden-lg hidden-sm hidden-xs\" data-entity-type=\"file\" data-entity-uuid=\"04c5ee0f-6dc7-4143-a798-6d6ade284969\" src=\"{{ field_slide_image_970_x_359_ }}\"></a><a href=\"{{ field_link_ }}\"><img alt=\"img3\" class=\"img-responsive hidden-md hidden-lg\" data-entity-type=\"file\" data-entity-uuid=\"24ff748d-9f21-42bf-9921-90cf0680de00\" src=\"{{ field_slide_image_750_x_359_ }}\"></a>

{% endif %}", "");
    }
}

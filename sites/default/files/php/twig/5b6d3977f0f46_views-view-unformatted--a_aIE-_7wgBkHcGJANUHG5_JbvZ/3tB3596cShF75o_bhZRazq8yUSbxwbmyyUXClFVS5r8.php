<?php

/* themes/iitbombayx/templates/views-view-unformatted--announcement_section.html.twig */
class __TwigTemplate_87f3fb057625dad28fefe4cd1b3d8654bdc80be5278f15e9ced63b4a3f98c9f1 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $tags = array("for" => 46, "if" => 48);
        $filters = array("trim" => 48, "render" => 48);
        $functions = array();

        try {
            $this->env->getExtension('Twig_Extension_Sandbox')->checkSecurity(
                array('for', 'if'),
                array('trim', 'render'),
                array()
            );
        } catch (Twig_Sandbox_SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof Twig_Sandbox_SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

        // line 45
        echo "
";
        // line 46
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["rows"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["row"]) {
            // line 47
            echo "
";
            // line 48
            if ((twig_trim_filter($this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar($this->getAttribute($context["row"], "content", array()))) == "")) {
            } else {
                // line 50
                echo "<section id=\"block-announcement\" class=\"block block-block-content clearfix\">
   <div class=\"alert alert-info alert-dismissible\" role=\"alert\" style=\"background-color: #fffacd; padding-top:0px;\">
     <button aria-label=\"Close\" class=\"close\" data-dismiss=\"alert\" type=\"button\"><span aria-hidden=\"true\">×</span>
     </button>
     <div class=\"text-center\">
\t\t<h6><span aria-hidden=\"true\" class=\"glyphicon glyphicon-bullhorn\"></span> Announcement</h6>

";
                // line 57
                echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute($context["row"], "content", array()), "html", null, true));
                echo "


</div>

</div>
      
  </section>
";
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['row'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
    }

    public function getTemplateName()
    {
        return "themes/iitbombayx/templates/views-view-unformatted--announcement_section.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  65 => 57,  56 => 50,  53 => 48,  50 => 47,  46 => 46,  43 => 45,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{#

/**
* This file is part of IITBombayX-Drupal.
*
* IITBombayX-Drupal is free software: you can redistribute it and/or modify it 
* under the terms of the GNU General Public License as published by the Free 
* Software Foundation, either version 3 of the License, or (at your option) any
* later version.
*
* IITBombayX-Drupal is distributed in the hope that it will be useful,but 
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
* FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
* more details.
*
* You should have received a copy of the GNU General Public License along with
* IITBombayX-Drupal.  If not, see <http://www.gnu.org/licenses/>.

*******************************************************************************
*                                                                             *
* Purpose: This customised view file is created for the display Announcement. *
*                                                                             *
* Created by: Mangesh Gharate                                                 *
*                                                                             *
*******************************************************************************
*/


/**
 * @file 
 * Theme override to display a view of unformatted rows.
 *
 * Available variables:
 * - title: The title of this group of rows. May be empty.
 * - rows: A list of the view's row items.
 *   - attributes: The row's HTML attributes.
 *   - content: The row's content.
 * - view: The view object.
 * - default_row_class: A flag indicating whether default classes should be
 *   used on rows.
 *
 * @see template_preprocess_views_view_unformatted()
 */
#}

{% for row in rows %}

{% if row.content|render|trim == ''   %}
{% else %}
<section id=\"block-announcement\" class=\"block block-block-content clearfix\">
   <div class=\"alert alert-info alert-dismissible\" role=\"alert\" style=\"background-color: #fffacd; padding-top:0px;\">
     <button aria-label=\"Close\" class=\"close\" data-dismiss=\"alert\" type=\"button\"><span aria-hidden=\"true\">×</span>
     </button>
     <div class=\"text-center\">
\t\t<h6><span aria-hidden=\"true\" class=\"glyphicon glyphicon-bullhorn\"></span> Announcement</h6>

{{ row.content }}


</div>

</div>
      
  </section>
{% endif %}
{% endfor %}
", "themes/iitbombayx/templates/views-view-unformatted--announcement_section.html.twig", "/var/www/html/IITBombayX/themes/iitbombayx/templates/views-view-unformatted--announcement_section.html.twig");
    }
}

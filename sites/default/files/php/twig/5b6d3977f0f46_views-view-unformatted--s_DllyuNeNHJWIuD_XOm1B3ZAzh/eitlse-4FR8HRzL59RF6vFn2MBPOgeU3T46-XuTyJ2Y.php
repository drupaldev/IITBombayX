<?php

/* themes/iitbombayx/templates/views-view-unformatted--slider-view.html.twig */
class __TwigTemplate_76f3108f02f630958739f26067a7f7100043379bdf363a66324304bdc1f5d123 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $tags = array("if" => 48, "set" => 54, "for" => 55);
        $filters = array();
        $functions = array();

        try {
            $this->env->getExtension('Twig_Extension_Sandbox')->checkSecurity(
                array('if', 'set', 'for'),
                array(),
                array()
            );
        } catch (Twig_Sandbox_SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof Twig_Sandbox_SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

        // line 47
        echo "
";
        // line 48
        if (($context["title"] ?? null)) {
            // line 49
            echo "  <h3>";
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["title"] ?? null), "html", null, true));
            echo "</h3>
";
        }
        // line 51
        echo "
<div class=\"carousel slide\" data-ride=\"carousel\" id=\"carousel-example-generic\"><!-- Indicators -->
<ol class=\"carousel-indicators\">
";
        // line 54
        $context["flag"] = 0;
        // line 55
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["rows"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["row"]) {
            // line 56
            echo "   ";
            if ((($context["flag"] ?? null) == 0)) {
                // line 57
                echo "\t<li class='active' data-slide-to=\"";
                echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["flag"] ?? null), "html", null, true));
                echo "\" data-target=\"#carousel-example-generic\">&nbsp;</li>
   ";
            } else {
                // line 59
                echo "        <li data-slide-to=\"";
                echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["flag"] ?? null), "html", null, true));
                echo "\" data-target=\"#carousel-example-generic\">&nbsp;</li>
   ";
            }
            // line 61
            echo " ";
            $context["flag"] = (($context["flag"] ?? null) + 1);
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['row'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 63
        echo "</ol>

<div class=\"carousel-inner\" role=\"listbox\">
";
        // line 66
        $context["oflag"] = 0;
        // line 67
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["rows"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["row"]) {
            // line 68
            echo "   ";
            if ((($context["oflag"] ?? null) == 0)) {
                // line 69
                echo "      <div class=\"item active\">
   ";
            } else {
                // line 71
                echo "      <div class=\"item\">
   ";
            }
            // line 73
            echo "   ";
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute($context["row"], "content", array()), "html", null, true));
            echo "
   
      </div>
   
   ";
            // line 77
            $context["oflag"] = (($context["oflag"] ?? null) + 1);
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['row'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 79
        echo "
</div>

<!-- Controls --><a class=\"left carousel-control\" data-slide=\"prev\" href=\"#carousel-example-generic\" role=\"button\"><span aria-hidden=\"true\" class=\"glyphicon glyphicon-chevron-left\"></span> <span class=\"sr-only\">Previous</span> </a> <a class=\"right carousel-control\" data-slide=\"next\" href=\"#carousel-example-generic\" role=\"button\"> <span aria-hidden=\"true\" class=\"glyphicon glyphicon-chevron-right\"></span> <span class=\"sr-only\">Next</span> </a></div>

";
    }

    public function getTemplateName()
    {
        return "themes/iitbombayx/templates/views-view-unformatted--slider-view.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  123 => 79,  117 => 77,  109 => 73,  105 => 71,  101 => 69,  98 => 68,  94 => 67,  92 => 66,  87 => 63,  80 => 61,  74 => 59,  68 => 57,  65 => 56,  61 => 55,  59 => 54,  54 => 51,  48 => 49,  46 => 48,  43 => 47,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{#


/**
* This file is part of IITBombayX-Drupal.
*
* IITBombayX-Drupal is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the Free
* Software Foundation, either version 3 of the License, or (at your option) any
* later version.
*
* IITBombayX-Drupal is distributed in the hope that it will be useful,but
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
* FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
* more details.
*
* You should have received a copy of the GNU General Public License along with
* IITBombayX-Drupal.  If not, see <http://www.gnu.org/licenses/>.

*******************************************************************************
*                                                                             *
* Purpose: This customised view file is created for the display Slides image. *
*                                                                             *
* Created by: Mangesh Gharate                                                 *
*                                                                             *
*******************************************************************************
*/



/**
 * @file 
 * Theme override to display a view of unformatted rows.
 *
 * Available variables:
 * - title: The title of this group of rows. May be empty.
 * - rows: A list of the view's row items.
 *   - attributes: The row's HTML attributes.
 *   - content: The row's content.
 * - view: The view object.
 * - default_row_class: A flag indicating whether default classes should be
 *   used on rows.
 *
 * @see template_preprocess_views_view_unformatted()
 */
#}

{% if title %}
  <h3>{{ title }}</h3>
{% endif %}

<div class=\"carousel slide\" data-ride=\"carousel\" id=\"carousel-example-generic\"><!-- Indicators -->
<ol class=\"carousel-indicators\">
{% set flag = 0 %}
{% for row in rows %}
   {% if (flag == 0) %}
\t<li class='active' data-slide-to=\"{{flag}}\" data-target=\"#carousel-example-generic\">&nbsp;</li>
   {% else %}
        <li data-slide-to=\"{{flag}}\" data-target=\"#carousel-example-generic\">&nbsp;</li>
   {% endif %}
 {% set flag = flag+1 %}
{% endfor %}
</ol>

<div class=\"carousel-inner\" role=\"listbox\">
{% set oflag = 0 %}
{% for row in rows %}
   {% if (oflag == 0) %}
      <div class=\"item active\">
   {% else %}
      <div class=\"item\">
   {% endif %}
   {{ row.content }}
   
      </div>
   
   {% set oflag = oflag+1 %}
{% endfor %}

</div>

<!-- Controls --><a class=\"left carousel-control\" data-slide=\"prev\" href=\"#carousel-example-generic\" role=\"button\"><span aria-hidden=\"true\" class=\"glyphicon glyphicon-chevron-left\"></span> <span class=\"sr-only\">Previous</span> </a> <a class=\"right carousel-control\" data-slide=\"next\" href=\"#carousel-example-generic\" role=\"button\"> <span aria-hidden=\"true\" class=\"glyphicon glyphicon-chevron-right\"></span> <span class=\"sr-only\">Next</span> </a></div>

", "themes/iitbombayx/templates/views-view-unformatted--slider-view.html.twig", "/var/www/html/IITBombayX/themes/iitbombayx/templates/views-view-unformatted--slider-view.html.twig");
    }
}
